# Clubs Management System
Pre-requisites
APACHE2 (UBUNTU)
UBUNTU/DEBIAN
• sudo apt update
• sudo apt install apache2
WINDOWS
• You have XAMP or WAMP installed. Everything is there for you A-Apache, M-MySQL,
P-PHP.
Other LINUX distros
• Google it, its almost same just package installer is different.
MySQL (UBUNTU)
UBUNTU
• sudo apt purge mysql* (Removes mysql completely)
• sudo apt install mysql-server mysql-client mysql-common
• sudo mysql_secure_installation (don’t rush before using this command)
• mysql -u root -p
WINDOWS
• you have it already.
Other LINUX distros
• Google it, its almost same just package installer is different.
PHP (UBUNTU)
UBUNTU
• sudo apt install php libapache2-mod-php php-mysql php-cli
• sudo systemctl restart apache2 or sudo service apache2 restartWINDOWS
• you have it already.
Other LINUX distros
• Google it, its almost same just package installer is different and also php versions are
different.
Steps for Installation of our Application
Linux(Any flavour)
1. Copy the zip format of the file to your system
2. Extract it to the directory /var/www/html/
3. Now create a database named cms in mysql.
4. Exit from mysql .
5. Now dump the database imsnew.sql into the system database by
typing the below command in the terminal:
mysql <options> cms < cms.sql
6. Now open any web browser then type localhost/cms/
7. You’ll get the homepage of the system deployed.
Windows
1. Copy the zip format of the file to the system.
2. Extract the zip file to the directory C://Xampp/htdocs/
3. Dump the cms.sql to the database.
4. Now open any web browser then type localhost/cms/
5. You’ll get the homepage of the system deployed.
Note :
1. Change the password of database with your database password in
config.php
2. You can change admin and users name, password in the database
table admin.